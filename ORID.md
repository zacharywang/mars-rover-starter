# ORID

## O (Objective)

- This morning focused on unit testing, where I learned how to write and run test cases to verify the correctness of the code. In the afternoon, I mainly learned about TDD, which guides development by writing test cases and iterating in a red-green-refactoring cycle to eventually refine the code.

- In the process of learning TDD, the basic concepts and methods of TDD were understood through explanation and practice. First write a test case and implement the code step by step according to the results defined in the test case. Until the test case passes. I was impressed by this way of driving development through testing.

- The most impressive part of the whole course is that in the practice of TDD, I myself improved the code through continuous iteration and gradually realized the expected functions. This is a very interesting and effective way of development. It made me understand the value and application scenarios of TDD more deeply.

## R (Reflective)

excited

## I (Interpretive)

Today's course has given me a deeper understanding of the basic concepts of TDD and the benefits of guiding development by writing tests first, which improves the maintainability and testability of the code.

## D (Decisional)

I would most like to apply what I learned today to my projects, especially by focusing more on quality and testability when writing code and conducting tests. I have decided to be more active in code review and unit testing, and to apply the TDD approach to my development practices in the future.



```

```