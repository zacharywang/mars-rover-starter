package com.afs.tdd;

import com.afs.tdd.command.MarsRoverCommandInterface;
import com.afs.tdd.command.*;

import java.util.List;

public class MarsRover {
    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }


    public Location executeCommand(Command command) {
        MarsRoverCommandInterface marsRoverCommand;
        switch (command) {
            case MOVE:
                marsRoverCommand = new MoveCommand();
                break;
            case LEFT:
                marsRoverCommand = new LeftCommand();
                break;
            case RIGHT:
                marsRoverCommand = new RightCommand();
                break;
            default:
                throw new IllegalArgumentException("命令无效");
        }
        this.location = marsRoverCommand.execute(this.location);
        return this.location;
    }

    public Location executeBatchCommands(List<Command> commands) {
        commands.forEach(this::executeCommand);
        return location;
    }
}
