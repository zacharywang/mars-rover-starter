package com.afs.tdd.command;

import com.afs.tdd.Direction;
import com.afs.tdd.Location;

public class LeftCommand implements MarsRoverCommandInterface {
    @Override
    public Location execute(Location location) {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.West);
                break;
            case South:
                location.setDirection(Direction.East);
                break;
            case East:
                location.setDirection(Direction.North);
                break;
            case West:
                location.setDirection(Direction.South);
                break;
        }
        return location;
    }
}
