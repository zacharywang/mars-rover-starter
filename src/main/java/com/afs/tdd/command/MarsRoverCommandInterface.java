package com.afs.tdd.command;

import com.afs.tdd.Location;

public interface MarsRoverCommandInterface {
    Location execute(Location location);
}
