package com.afs.tdd.command;

import com.afs.tdd.Location;

public class MoveCommand implements MarsRoverCommandInterface{
    private Location newLocation;
    @Override
    public Location execute(Location oldLocation) {
        newLocation = new Location(oldLocation.getCoordinateX(),oldLocation.getCoordinateY(),oldLocation.getDirection());
        switch (oldLocation.getDirection()) {
            case North:
                newLocation.setCoordinateY(newLocation.getCoordinateY() + 1);
                break;
            case South:
                newLocation.setCoordinateY(newLocation.getCoordinateY() - 1);
                break;
            case East:
                newLocation.setCoordinateX(newLocation.getCoordinateX() + 1);
                break;
            case West:
                newLocation.setCoordinateX(newLocation.getCoordinateX() - 1);
                break;
        }
        return newLocation;
    }
}
