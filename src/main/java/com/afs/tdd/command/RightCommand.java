package com.afs.tdd.command;

import com.afs.tdd.Direction;
import com.afs.tdd.Location;

public class RightCommand implements MarsRoverCommandInterface{
    @Override
    public Location execute(Location location) {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.East);
                break;
            case South:
                location.setDirection(Direction.West);
                break;
            case East:
                location.setDirection(Direction.South);
                break;
            case West:
                location.setDirection(Direction.North);
                break;
        }
        return location;
    }
}
