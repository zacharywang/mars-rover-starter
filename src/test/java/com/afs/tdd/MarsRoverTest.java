package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MarsRoverTest {
    @Test
    void should_change_to_0_1_North_when_executeCommand_given_location_0_0_N_and_command_move() {
        //given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_negative_1_South_when_executeCommand_given_location_0_0_S_and_command_move() {
        //given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_1_0_East_when_executeCommand_given_location_0_0_E_and_command_move() {
        //given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_negative_1_0_West_when_executeCommand_given_location_0_0_W_and_command_move() {
        //given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_N_and_command_left() {
        //given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.LEFT;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_E_and_command_left() {
        //given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.LEFT;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_N_and_command_left() {
        //given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.LEFT;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_S_and_command_left() {
        //given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.LEFT;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_N_and_command_right() {
        //given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.RIGHT;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_S_and_command_right() {
        //given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.RIGHT;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_E_and_command_right() {
        //given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandRight = Command.RIGHT;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandRight);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_W_and_command_right() {
        //given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.RIGHT;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_N_and_batch_commands() {
        //given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        List<Command> commands = new ArrayList<>();
        commands.add(Command.LEFT);
        commands.add(Command.MOVE);
        commands.add(Command.LEFT);
        commands.add(Command.MOVE);
        commands.add(Command.RIGHT);
        commands.add(Command.MOVE);


        //when
        Location locationAfterBatchCommand = marsRover.executeBatchCommands(commands);
        //Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(-2, locationAfterBatchCommand.getCoordinateX()),
                () -> Assertions.assertEquals(-1, locationAfterBatchCommand.getCoordinateY()),
                () -> Assertions.assertEquals(Direction.West, locationAfterBatchCommand.getDirection())
        );

    }
}
